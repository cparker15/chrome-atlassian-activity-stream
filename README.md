# EAC (extranet.atlassian.com) Activity Stream for Chrome

I got tired of going to to EAC everytime to look at the activity stream so I whipped up this simple little Chrome extension that allows you to view the latest activities from EAC. As a bonus, it also allows you to update your status. You can install it from **[here](https://extranet.atlassian.com/download/attachments/1940332086/eac_activitystream.crx)**.

![Screenshot](https://img.skitch.com/20110913-8xdhr8fgabk5q3cid6ajke74yk.png)

## Issues?

Either send them to <rmanalang@atlassian.com> or fork this repo, add your fix or enhancement, then send me a pull request.
