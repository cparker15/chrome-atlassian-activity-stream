(function(){function c(a){this.t=a}function l(a,b){for(var e=b.split(".");e.length;){if(!(e[0]in a))return!1;a=a[e.shift()]}return a}function d(a,b){return a.replace(h,function(e,a,i,f,c,h,k,m){var f=l(b,f),j="",g;if(!f)return"!"==i?d(c,b):k?d(m,b):"";if(!i)return d(h,b);if("@"==i){e=b._key;a=b._val;for(g in f)f.hasOwnProperty(g)&&(b._key=g,b._val=f[g],j+=d(c,b));b._key=e;b._val=a;return j}}).replace(k,function(a,c,d){return(a=l(b,d))||0===a?"%"==c?(new Option(a)).innerHTML.replace(/"/g,"&quot;"):
a:""})}var h=/\{\{(([@!]?)(.+?))\}\}(([\s\S]+?)(\{\{:\1\}\}([\s\S]+?))?)\{\{\/\1\}\}/g,k=/\{\{([=%])(.+?)\}\}/g;c.prototype.render=function(a){return d(this.t,a)};window.t=c})();

$.extend($.infinitescroll.prototype,{
  _loadcallback_custom: function infscr_callback_custom (e,d) {
    $.each(d.streamItems, function(){
      $('#entries').append((new t($('#entry-tmpl').html())).render(this));
    });
    $('#infscr-loading').remove();
    $('#next-page').attr('href','https://extranet.atlassian.com/rest/popular/1/stream/content?days=1&pageSize=10&nextPageOffset=' + d.nextPageOffset);
    $('.navigation').show();
    this.options.state.isDuringAjax = false;
  }
});

$(function(){
  // Hack to allow all hyperlinks to open in a tab
  $('a').live('click',function(){
    $(this).attr('target','_blank');
  });

  // Get last updated status
  $.get('https://extranet.atlassian.com/status/current.action',function(d){
    $('#last-update').empty().append((new t($('#last-update-tmpl').html())).render(d));
    if(/^<!DOCTYPE/.test(arguments[2].responseText)) {
      chrome.tabs.create({
        url: 'https://extranet.atlassian.com'
      });
      window.close();
    }
  });

  // Get latest activity stream from activity stream feed
  // $.get('https://extranet.atlassian.com/spaces/createrssfeed.action?types=page&amp;amp;types=blogpost&amp;amp;types=comment&amp;amp;spaces=&amp;amp;sort=modified&amp;amp;title=Dashboard+RSS+Feed&amp;amp;maxResults=15&amp;amp;publicFeed=false&amp;amp;os_authType=basic&amp;amp;rssType=atom',function(d){
  //   var feed = $.xml2json(d);
  //   if(!feed.entry) return false;
  //   $.each(feed.entry, function(){
  //     var entry = this,
  //         data = {
  //           photo: entry.author.link[1].href,
  //           entryTitle: entry.title.text,
  //           summary: entry.summary ? entry.summary.text : entry.content.text,
  //           since: Date.create(entry.updated).relative()
  //         };
  //     $('#entry-tmpl').tmpl(data).appendTo('#entries');
  //   });
  // });

  $('body').delegate('.link','click',function(){
    //alert($(this).attr('href'));
    console.log('click',this,$(this).attr('href'))
    chrome.tabs.create({
      url: $(this).attr('href'),
      active: false
    });
    $(this).addClass('disabled');
    return false;
  });

  $.get('https://extranet.atlassian.com/rest/popular/1/stream/content?days=1&pageSize=20',function(d){
    if(d.streamItems.length === 0) return false;
    $('div.navigation a:first').attr('href','https://extranet.atlassian.com/rest/popular/1/stream/content?days=1&pageSize=20&nextPageOffset=' + d.nextPageOffset);
    $.each(d.streamItems, function(){
      $('#entries').append((new t($('#entry-tmpl').html())).render(this));
    });

    $('#entries').infinitescroll({
      //itemSelector: "li.row",
      loading: {
        finishedMsg: "",
        finished: function(){
          console.log('finished', arguments)
        }
      },
      behavior: 'custom',
      nextSelector: "#next-page",
      dataType: 'json',
      template: function() { return new t($('#entry-tmpl').html()).render },
      pathParse: function(url){
        console.log(arguments)
        return ["https://extranet.atlassian.com/rest/popular/1/stream/content?days=1&pageSize=10&nextPageOffset="];
      },
      appendCallback:false,
      errorCallback: function(d){
        console.log('errorCallback', arguments)
      }
    },function(d){
      console.log('DONE',arguments)
    });
  }).done(function(d){console.log(d)});


  // Set up status update button
  $('#update-status-action').bind('click',function(){
    var status = $('#update-status textarea').val();
    if (!status) return false;

    // Hack to get the required atlassian token necessary to submit the updated status
    $.get('https://extranet.atlassian.com/users/viewmyprofile.action',function(d){
      var atlToken = $(d).filter('#atlassian-token').attr('content');

      // Send status update
      $.ajax({
        url: 'https://extranet.atlassian.com/status/update.action',
        type: 'post',
        dataType: 'json',
        data: {text:status, atl_token: atlToken}
      }).success(function(d){

         // Clear out status text area then set "Last update" text to be the new status
         $('#update-status textarea').val('');
         $('#last-update').empty().append($('#last-update-tmpl').tmpl(d));
      });
    });
  });
});
